package com.example;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOError;
import java.io.IOException;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("myresource")
public class MyResource {

    SearchUser suerList = new SearchUser();

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */

    @GET
    @Path("/users/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getIt(@PathParam("id") Integer id) {

        try{
            throw new IOException("da");
        }
        catch (Exception e){

        }
        return "Got It";
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public User addUser(User user){
        suerList.addUser(user);
        return user;

    }
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public User updateUser(User user){
        return user;
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public User deleteUser(User user){
        return user;
    }

}
