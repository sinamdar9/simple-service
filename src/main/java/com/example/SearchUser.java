package com.example;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sinamdar on 12/16/18.
 */
public class SearchUser {

    public List<User> userList;

    public SearchUser(){
        userList = new ArrayList<>();
    }

    public void addUser(User user){
        userList.add(user);
    }

    public User getUserDetails(int id){
        for(User u :userList){
            if(u.getId()==id){
                return u;
            }
        }
        return null;
    }

}
